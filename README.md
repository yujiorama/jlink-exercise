jlink exercise
====

## TL;DR

* [Simon Ritter さんの Medium](https://medium.com/azulsystems/using-jlink-to-build-java-runtimes-for-non-modular-applications-9568c5e70ef4) を読んで、自分でも試してみようと思った
* 少し実用を考慮して [Spring Initilizr](https://start.spring.io) で Web とか Security とか Spring Data JPA とかを指定したプロジェクトを使用した
* `jdeps` では必要なモジュールを特定できなかった
* 実行時にエラーとして出力した例外スタックトレースの `ClassDefNotFound` から必要なクラスとそれが属するモジュールを推測してやりくりした
* adoptopenjdk でビルドして alpine で動かす、ということは実現できなかった

## Maven プロジェクトの作成

[Spring Initilizr](https://start.spring.io) で操作して作成。

`pom.xml` の内容が気になったのでいろいろ追加。

`./mvnw clean package` でビルドする。

`./mvnw spring-boot:run -Dspring.datasource.url=jdbc:mysql://localhost:3306/demodb?useSSL=false` でなんか動作することは分かる状態。

次の条件で MySQL を動かしておくこと。

| パラメータ | 値 |
|:----------:|:--:|
| ホスト名 | localhost |
| 接続ポート番号 | 3306 |
| データベース名 | demodb |
| 接続ユーザー名 | springuser |
| 接続パスワード | ThePassword |

`curl -u actuator:password http://localhost:8080/actuator/` で Actuator のエンドポイントにアクセスできる。

`curl http://localhost:8080/demo/all` でデータベースに登録したデータを閲覧できる。

`curl http://localhost:8080/demo/add?name=foo\&email=bar@example.com` でデータベースにデータを登録できる。


## ディストリビューションの作成

ディストリビューションは [adoptopenjdk/openjdk11:jdk-11.28](https://github.com/AdoptOpenJDK/openjdk-docker/blob/master/11/jdk/ubuntu/Dockerfile.hotspot.releases.full) でビルドし、元にしている [Ubuntu のイメージ](https://github.com/tianon/docker-brew-ubuntu-core/blob/222130dfdfa777c09a17b3f08ba68c5b9850e905/xenial/Dockerfile) で実行することにした。

```bash
docker build -f Dockerfile -t jlink-exercise:ubuntu-16.04 .
docker build -f Dockerfile.alpine -t jlink-exercise:alpine-3.8 .
```

利用したイメージの大きさは次のとおり。

| image | size  |
|:-----:|:-----:|
| adoptopenjdk/openjdk11:jdk-11.28 (c450f3f49896) | 454 MB |
| ubuntu:16.04 (b9e15a5d1e1a) | 115 MB |
| alpine:3.8 (196d12cf6ab1) | 4.41 MB |

ビルドしたイメージの大きさは次のとおり。

| image | size  |
|:-----:|:-----:|
| jlink-exercise:ubuntu-16.04 (82ab239b8133) | 205 MB |
| jlink-exercise:alpine-3.8 (8d79e49b023f) | 95.8 MB |

ubnutu:16.04 に配置したほうは 205 MB。adoptopenjdk のイメージと比べたら半分くらいなのでわりといい感じ。

alpine:3.8 に配置したほうは 95.8 MB。alpine にしては大きいけど adoptopenjdk のイメージと比べたら25%なので十分小さい（ことにする）。

本当は alpine で実行したかったのだけど、いろいろと構造が違ってるせいで騙しきれなかった。つまり起動しない。

```bash
$ docker container run --rm -it --link mysql:db --publish 18080:8080 jlink-exercise:alpine-3.8 sh
~ $ ./dist/bin/java --version
Error relocating /work/dist/bin/../lib/jli/libjli.so: __rawmemchr: symbol not found
~ $ ldd dist/bin/java
        /lib64/ld-linux-x86-64.so.2 (0x7f6289357000)
        libz.so.1 => /lib/libz.so.1 (0x7f6289140000)
        libpthread.so.0 => /lib64/ld-linux-x86-64.so.2 (0x7f6289357000)
        libjli.so => dist/bin/../lib/jli/libjli.so (0x7f6288f2f000)
        libdl.so.2 => /lib64/ld-linux-x86-64.so.2 (0x7f6289357000)
        libc.so.6 => /lib64/ld-linux-x86-64.so.2 (0x7f6289357000)
Error relocating dist/bin/../lib/jli/libjli.so: __rawmemchr: symbol not found
```
