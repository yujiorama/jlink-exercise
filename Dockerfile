FROM adoptopenjdk/openjdk11:jdk-11.28 as builder
ARG WORKDIR=/work
WORKDIR ${WORKDIR}
COPY . ${WORKDIR}/
RUN set -x \
 && find ./src -type d -exec chmod 755 {} \; \
 && find ./src -type f -exec chmod 644 {} \; \
 && chmod 644 ./pom.xml \
 && chmod 755 ./mvnw \
 && ./mvnw clean package \
 && jlink \
    --verbose \
    --no-header-files \
    --no-man-pages \
    --compress=2 \
    --strip-debug \
    --add-modules java.base,java.logging,java.sql,java.naming,java.desktop,java.management,java.security.jgss,java.instrument,jdk.unsupported \
    --output target/dist

FROM ubuntu:16.04
ARG WORKDIR=/work
ARG USER=appuser
RUN set -x \
 && adduser --home ${WORKDIR} --gecos '' --ingroup users --disabled-password ${USER}
USER ${USER}
WORKDIR ${WORKDIR}
COPY --from=builder ${WORKDIR}/target/dist ${WORKDIR}/dist/
COPY --from=builder ${WORKDIR}/target/*.jar ${WORKDIR}/
ENV JAVA_HOME=${WORKDIR}/dist
ENV PATH=${JAVA_HOME}/bin:${PATH}
ENV SERVER_PORT=8080
EXPOSE ${SERVER_PORT}
ENTRYPOINT ["/bin/bash", "-c"]
CMD ["./dist/bin/java", "-jar", "./demo-0.0.1-SNAPSHOT.jar", "--server.port=${SERVER_PORT}"]
